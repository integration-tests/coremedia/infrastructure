
variable "servers" {
  type = map(map(string))
  default = {
    "jenkins" = {
      memory    = 2048
      vcpu      = 2
      disk_size = "8589934592"
      octetIP     = 10
      hostname  = "jenkins"
    }
    "registry" = {
      memory    = 1024
      vcpu      = 1
      disk_size = "8589934592"
      octetIP   = 11
      hostname  = "registry"
    }
  }
}
