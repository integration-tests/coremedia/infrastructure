
resource "libvirt_volume" "debian11_image" {
  name     = "debian-image-11"
  pool     = "pool"
  source   = "/var/lib/libvirt/pool/debian-11-amd64.qcow2"
}

resource "libvirt_volume" "debian12_image" {
  name     = "debian-image-12"
  pool     = "pool"
  source   = "/var/lib/libvirt/pool/debian-12-amd64.qcow2"
}

resource "libvirt_volume" "diskimage" {

  for_each       = var.servers

  pool           = "default"
  name           = "${each.key}.qcow2"
  # base_volume_id = "libvirt_volume.${lookup(var.servers[each.key], "base_image", "debian12")}_image.id"
  base_volume_id = libvirt_volume.debian11_image.id
  size           = lookup(var.servers[each.key], "disk_size", "") == "" ? "0" : var.servers[each.key].disk_size
}
