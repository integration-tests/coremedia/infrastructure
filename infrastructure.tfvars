
# 60 GiB = 64424509440
# 20 GiB = 21474836480
# 10 GiB = 10737418240
#  8 GiB = 8589934592
#  2 GiB = 2524971008

servers = {

  dns = {
    "memory"    = 512
    "vcpu"      = 1
    "disk_size" = "2524971008"
    octetIP     = 5
    "hostname"  = "cm-dns"
  }
  build = { # 60 GiB
    "memory"    = 12288
    "vcpu"      = 3
    "disk_size" = "64424509440"
    octetIP     = 10
    "hostname"  = "cm-build"
    base_image  = "debian11"
  }
  # dba_frontend = {
  #   "memory"    = 2048
  #   "disk_size" = 8589934592
  #   "vcpu"      = 1
  #   "octetIP"   = 100
  #   "hostname"  = "cm-dba-frontend"
  # }
  # dba_backend = {
  #   "memory"    = 2048
  #   "disk_size" = 8589934592
  #   "vcpu"      = 1
  #   "octetIP"   = 110
  #   "hostname"  = "cm-dba-backend"
  # }
  # studio = {
  #   "memory"    = 4096
  #   "vcpu"      = 1
  #   "disk_size" = 8589934592
  #   "octetIP"   = 20
  #   "hostname"  = "cm-studio"
  # }
  # cms = {
  #   "memory"    = 10240
  #   "vcpu"      = 2
  #   "disk_size" = 8589934592
  #   "octetIP"   = 21
  #   "hostname"  = "cm-cms"
  # }
  # mls = {
  #   "memory"    = 4096
  #   "vcpu"      = 2
  #   "disk_size" = 8589934592
  #   "octetIP"   = 30
  #   "hostname"  = "cm-mls"
  # }
  # rls_1 = {
  #   "memory"    = 6144
  #   "vcpu"      = 1
  #   "disk_size" = 8589934592
  #   "octetIP"   = 35
  #   "hostname"  = "cm-rls-01"
  # }
  #
  # monitoring = {
  #   "memory"    = 4096
  #   "vcpu"      = 1
  #   "disk_size" = 8589934592
  #   "octetIP"   = 50
  #   "hostname"  = "cm-monitoring"
  # }
}
